
const FIRST_NAME = "SORINA";
const LAST_NAME = "SERBAN";
const GRUPA = "1083";

/**
 * Make the implementation here
 */
function numberParser(value) {
    if (value<Number.MIN_SAFE_INTEGER || value>Number.MAX_SAFE_INTEGER){
        return NaN;
    }
    return parseInt(value);
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

